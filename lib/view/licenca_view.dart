import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/models/raw_credentials.dart';
import 'package:my_green_house/view/components/padding.dart';
import 'package:my_green_house/view/components/text.dart';
import 'package:my_green_house/view/components/vertical_spacer.dart';

import '../models/exception.dart';
import '../services/auth_service.dart';
import 'components/button.dart';
import 'components/checkbox.dart';

/// Página que pede ao usuário concordar com a licença
/// para criar sua conta.
class LicencaView extends StatefulWidget {
  final RawCredentials credentials;
  const LicencaView({Key? key, required this.credentials}) : super(key: key);

  @override
  State<LicencaView> createState() => _LicencaViewState();
}

class _LicencaViewState extends State<LicencaView> {
  bool leuContrato = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green.shade100,
        iconTheme: IconThemeData(color: Colors.grey.shade600),
        elevation: 0,
      ),
      backgroundColor: Colors.green.shade400,
      body: GreenHousePadding(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const GreenHouseText(
              text: "Licença de uso",
              size: 35,
            ),
            const GreenHouseVerticalSpacer(),
            const GreenHouseCheckBox(
              label:
                  'Essa tecnologia é para uso acadêmico para fins de conhecimento. Não é permitido uso comercial desse aplicativo.',
              value: true,
            ),
            const GreenHouseVerticalSpacer(),
            GreenHouseCheckBox(
              label: 'Eu li e aceito o contrato de licença de uso',
              value: leuContrato,
              onChanged: (bool newValue) {
                setState(() {
                  leuContrato = newValue;
                });
              },
            ),
            const GreenHouseVerticalSpacer(),
            GreenHouseButton(
              color: Colors.brown,
              onPressed: () async {
                try {
                  final authService = Get.find<AuthService>();
                  if (leuContrato) {
                    await authService.signUp(
                        widget.credentials.email, widget.credentials.password);
                  } else {
                    throw const GreenHouseException(
                        title: 'Aceite o contrato',
                        message: 'Aceite o contrato para usar o app.');
                  }
                } on GreenHouseException catch (err) {
                  Get.snackbar(err.title, err.message);
                }
              },
              buttonText: "Completar seu registro",
            ),
          ],
        ),
      ),
    );
  }
}
