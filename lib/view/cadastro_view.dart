import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/models/raw_credentials.dart';
import 'package:my_green_house/view/components/padding.dart';
import 'package:my_green_house/view/licenca_view.dart';

import 'components/button.dart';
import 'components/text.dart';
import 'components/vertical_spacer.dart';
import 'components/text_field.dart';

class CadastroView extends StatelessWidget {
  final _emailController = TextEditingController();
  final _senhaController = TextEditingController();
  final _senhaConfirmaController = TextEditingController();

  CadastroView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green.shade100,
        iconTheme: IconThemeData(color: Colors.grey.shade600),
        elevation: 0,
      ),
      backgroundColor: Colors.green.shade400,
      body: GreenHousePadding(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const GreenHouseText(text: "Crie sua conta", size: 35),
              const GreenHouseVerticalSpacer(
                height: 30,
              ),
              const GreenHouseText(text: "E-mail"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(controller: _emailController),
              const GreenHouseVerticalSpacer(),
              const GreenHouseText(text: "Senha"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(
                  controller: _senhaController, isPassword: true),
              const GreenHouseVerticalSpacer(),
              const GreenHouseText(text: "Confirme sua senha"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(
                  controller: _senhaConfirmaController, isPassword: true),
              const GreenHouseVerticalSpacer(height: 40),
              GreenHouseButton(
                  color: Colors.brown,
                  onPressed: () {
                    if (_senhaController.text.isEmpty ||
                        _senhaConfirmaController.text.isEmpty) {
                      Get.snackbar('Erro', 'Senha em branco');
                      return;
                    }
                    if (_senhaController.text !=
                        _senhaConfirmaController.text) {
                      Get.snackbar('Erro', 'Senhas digitadas não correspondem');
                      return;
                    }

                    Get.to(
                      () => LicencaView(
                        credentials: RawCredentials(
                            _emailController.text, _senhaController.text),
                      ),
                    );
                  },
                  buttonText: "Prosseguir!")
            ],
          ),
        ),
      ),
    );
  }
}
