import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/view/cadastro_view.dart';
import 'package:my_green_house/view/components/button.dart';
import 'package:my_green_house/view/components/padding.dart';
import 'package:my_green_house/view/components/vertical_spacer.dart';
import 'package:my_green_house/view/login_view.dart';

/// Página inicial onde o usuário pode fazer login
/// ou se cadastrar.
class LandingView extends StatelessWidget {
  const LandingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GreenHousePadding(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Flexible(child: Center(child: Image.asset("images/green.png"))),
            GreenHouseButton(
                color: Colors.brown,
                onPressed: () {
                  Get.to(() => LoginView());
                },
                buttonText: "Entrar"),
            const GreenHouseVerticalSpacer(),
            GreenHouseButton(
                color: Colors.green.shade400,
                onPressed: () {
                  Get.to(() => CadastroView());
                },
                buttonText: "Crie sua Conta!")
          ],
        ),
      ),
    );
  }
}
