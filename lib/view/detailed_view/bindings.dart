import 'package:get/get.dart';
import './detailed_view.dart';
import '../../controllers/detailed_view_controller.dart';

/// Classe que detalha as dependências da página [DetailedView]
class DetailedViewBindings implements Bindings {
  /// Id da controladora, será passado para uma instância de [DetailedViewController]
  String contrId;
  DetailedViewBindings(this.contrId);
  @override
  void dependencies() {
    // Inicia uma instância do controlador dessa view, DetailedViewController.
    Get.put(DetailedViewController(contrId));
  }
}
