import 'package:flutter/material.dart';

/// Card que mostra detalhes da controladora.
/// [icon] e [iconLabel] são opcionais.
class DetailCard extends StatelessWidget {
  const DetailCard({
    Key? key,
    required this.circleColor,
    required this.circleLabel,
    required this.label,
    this.icon,
    this.iconLabel,
    required this.switchValue,
    required this.onSwitchChange,
  }) : super(key: key);

  final Color circleColor;
  final String circleLabel;
  final String label;

  final Widget? icon;
  final String? iconLabel;

  final bool switchValue;
  final void Function(bool) onSwitchChange;

  @override
  Widget build(BuildContext context) {
    const circleSize = 35.0;
    const containerSize = 70.0;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: LayoutBuilder(builder: (context, constraints) {
        return Stack(
          children: [
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                height: containerSize,
                width: constraints.maxWidth - (circleSize / 2),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
                child: Row(
                  children: [
                    const SizedBox(
                      width: circleSize,
                    ),
                    Text(
                      label,
                      style: const TextStyle(fontSize: 20),
                    ),
                    const Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        if (icon != null) icon!,
                        if (iconLabel != null)
                          Text(
                            iconLabel!,
                            style: const TextStyle(
                              fontSize: 20,
                              color: Colors.blue,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        Switch(value: switchValue, onChanged: onSwitchChange)
                      ],
                    ),
                    const SizedBox(
                      width: circleSize / 2,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: (containerSize / 2) - (circleSize / 2),
              child: Container(
                width: circleSize,
                height: circleSize,
                decoration: BoxDecoration(
                  color: circleColor,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(90),
                  ),
                ),
                child: Center(
                  child: Text(circleLabel,
                      style: const TextStyle(color: Colors.white)),
                ),
              ),
            ),
          ],
        );
      }),
    );
  }
}
