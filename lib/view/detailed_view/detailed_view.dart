import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/controllers/detailed_view_controller.dart';
import 'package:my_green_house/view/components/card.dart';
import 'package:my_green_house/view/components/padding.dart';
import 'package:my_green_house/view/detailed_view/components/detail_card.dart';

/// Página que mostra detalhes da controladora.
class DetailedView extends GetView<DetailedViewController> {
  const DetailedView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green.shade100,
        iconTheme: IconThemeData(color: Colors.grey.shade600),
        elevation: 0,
      ),
      backgroundColor: Colors.green.shade400,
      body: Obx(
        () => controller.microController.value == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : GreenHousePadding(
                child: ListView(
                  children: [
                    GreenHouseCard(
                      microController: controller.microController.value!,
                      onPressed: () {},
                      shouldShowWorking: false,
                    ),
                    DetailCard(
                        circleColor: Colors.green,
                        circleLabel: '1',
                        label: 'Temperatura',
                        iconLabel:
                            controller.microController.value!.state!.temp,
                        switchValue: true,
                        onSwitchChange: (v) {}),
                    DetailCard(
                        circleColor: Colors.yellow.shade600,
                        circleLabel: '2',
                        label: 'Umidade',
                        icon: const Icon(
                          Icons.water_drop,
                          color: Colors.blue,
                          size: 35,
                        ),
                        iconLabel:
                            controller.microController.value!.state!.humidity,
                        switchValue: true,
                        onSwitchChange: (v) {}),
                    DetailCard(
                        circleColor: Colors.green,
                        circleLabel: '3',
                        label: 'Janela',
                        switchValue:
                            controller.microController.value!.state!.janela,
                        onSwitchChange: (v) {}),
                    DetailCard(
                        circleColor: Colors.yellow.shade600,
                        circleLabel: '4',
                        label: 'Irrigação',
                        switchValue:
                            controller.microController.value!.state!.irrigacao,
                        onSwitchChange: (v) {}),
                  ],
                ),
              ),
      ),
    );
  }
}
