import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/models/exception.dart';
import 'package:my_green_house/services/auth_service.dart';
import 'package:my_green_house/view/components/padding.dart';

import 'components/button.dart';
import 'components/text.dart';
import 'components/vertical_spacer.dart';
import 'components/text_field.dart';

/// Página de login.
class LoginView extends StatelessWidget {
  final _emailController = TextEditingController();
  final _senhaController = TextEditingController();
  LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green.shade100,
        iconTheme: IconThemeData(color: Colors.grey.shade600),
        elevation: 0,
      ),
      backgroundColor: Colors.green.shade400,
      body: GreenHousePadding(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const GreenHouseText(text: "Login", size: 35),
              const GreenHouseVerticalSpacer(height: 30),
              const GreenHouseText(text: "E-mail"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(controller: _emailController),
              const GreenHouseVerticalSpacer(),
              const GreenHouseText(text: "Senha"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(
                controller: _senhaController,
                isPassword: true,
              ),
              const GreenHouseVerticalSpacer(height: 40),
              GreenHouseButton(
                color: Colors.brown,
                onPressed: () async {
                  try {
                    final authService = Get.find<AuthService>();
                    await authService.signIn(
                        _emailController.text, _senhaController.text);
                  } on GreenHouseException catch (err) {
                    Get.snackbar(err.title, err.message);
                  }
                },
                buttonText: "Entre!",
              )
            ],
          ),
        ),
      ),
    );
  }
}
