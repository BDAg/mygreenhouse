import 'package:flutter/material.dart';

/// Padding padrão do app. É possível customizar.
class GreenHousePadding extends StatelessWidget {
  const GreenHousePadding({
    Key? key,
    required this.child,
    // this.padding = const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
    this.padding = const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
  }) : super(key: key);
  final Widget child;
  final EdgeInsets padding;
  @override
  Widget build(BuildContext context) {
    return Padding(padding: padding, child: child);
  }
}
