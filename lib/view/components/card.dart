import 'package:flutter/material.dart';
import 'package:my_green_house/models/micro_controller.dart';
import 'package:my_green_house/view/components/vertical_spacer.dart';

/// Card que mostra o status da controladora
class GreenHouseCard extends StatelessWidget {
  const GreenHouseCard({
    Key? key,
    required this.microController,
    required this.onPressed,
    this.shouldShowWorking = true,
  }) : super(key: key);
  final GreenHouseMicroController microController;
  final VoidCallback onPressed;
  final bool shouldShowWorking;
  @override
  Widget build(BuildContext context) {
    final color = microController.isWorking ? Colors.blue : Colors.grey;
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
              color: color.withOpacity(0.4),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
          child: Column(
            children: [
              Text(
                microController.name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              if (shouldShowWorking) ...[
                const GreenHouseVerticalSpacer(),
                Text(
                  microController.isWorking ? "Em Funcionamento" : "Offline",
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ]
            ],
          ),
        ),
      ),
    );
  }
}
