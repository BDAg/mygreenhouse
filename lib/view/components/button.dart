import 'package:flutter/material.dart';

/// Botão padrão do app.
class GreenHouseButton extends StatelessWidget {
  const GreenHouseButton(
      {Key? key,
      required this.color,
      required this.onPressed,
      required this.buttonText})
      : super(key: key);
  final Color color;
  final VoidCallback onPressed;
  final String buttonText;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0),
      child: ElevatedButton(
          onPressed: onPressed,
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(color),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40),
              ),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              buttonText,
              style: const TextStyle(fontSize: 25),
            ),
          )),
    );
  }
}
