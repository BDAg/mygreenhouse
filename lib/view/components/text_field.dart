import 'package:flutter/material.dart';

/// Textfield do App.
///
/// [controller] é necessário para pegar os valores
/// do text field.
///
/// [isPassword] indica se
/// o textfield precisa ocultar os caractéres
/// ou não.
class GreenHouseTextField extends StatelessWidget {
  final bool isPassword;
  const GreenHouseTextField(
      {Key? key, this.controller, this.isPassword = false})
      : super(key: key);
  final TextEditingController? controller;
  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
      ),
      controller: controller,
      obscureText: isPassword,
    );
  }
}
