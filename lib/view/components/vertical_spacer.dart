import 'package:flutter/material.dart';

/// Espaçador vertical. Pode mudar o espaçamento alterando [height]
class GreenHouseVerticalSpacer extends StatelessWidget {
  const GreenHouseVerticalSpacer({Key? key, this.height = 20})
      : super(key: key);
  final double height;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
    );
  }
}
