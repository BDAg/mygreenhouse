import 'package:flutter/material.dart';

/// Widget que renderiza o texto, com estilo pré aplicado.
class GreenHouseText extends StatelessWidget {
  const GreenHouseText({Key? key, required this.text, this.size = 30})
      : super(key: key);
  final String text;
  final double size;
  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(fontSize: size));
  }
}
