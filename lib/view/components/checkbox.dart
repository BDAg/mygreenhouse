import 'package:flutter/material.dart';
import 'package:my_green_house/view/components/text.dart';

/// Widget que contém uma checkbox com label
class GreenHouseCheckBox extends StatelessWidget {
  const GreenHouseCheckBox({
    Key? key,
    required this.label,
    this.padding = const EdgeInsets.symmetric(vertical: 15),
    required this.value,
    this.onChanged,
  }) : super(key: key);

  final String label;
  final EdgeInsets padding;
  final bool value;
  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onChanged != null) {
          onChanged!(!value);
        }
      },
      child: Padding(
        padding: padding,
        child: Row(
          children: <Widget>[
            Expanded(
                child: GreenHouseText(
              text: label,
              size: 20,
            )),
            Checkbox(
              value: value,
              onChanged: (bool? newValue) {
                if (onChanged != null && newValue != null) {
                  onChanged!(!value);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
