import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/models/exception.dart';
import 'package:my_green_house/models/micro_controller.dart';
import 'package:my_green_house/services/auth_service.dart';
import 'package:my_green_house/services/micro_controller_service.dart';
import 'package:my_green_house/view/components/button.dart';
import 'package:my_green_house/view/components/padding.dart';

import 'components/text.dart';
import 'components/text_field.dart';
import 'components/vertical_spacer.dart';

/// Página que adiciona uma controladora
class AddControllerView extends StatelessWidget {
  AddControllerView({Key? key}) : super(key: key);
  final _nomeController = TextEditingController();
  final _hostController = TextEditingController();
  final _portaController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green.shade100,
          iconTheme: IconThemeData(color: Colors.grey.shade600),
          elevation: 0,
        ),
        backgroundColor: Colors.green.shade400,
        body: GreenHousePadding(
          child: ListView(
            children: [
              const GreenHouseText(text: "Login", size: 35),
              const GreenHouseVerticalSpacer(height: 30),
              const GreenHouseText(text: "Nomeie seu aparelho"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(controller: _nomeController),
              const GreenHouseVerticalSpacer(),
              const GreenHouseText(text: "IP do Host - ESP32"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(controller: _hostController),
              const GreenHouseVerticalSpacer(),
              const GreenHouseText(text: "Porta"),
              const GreenHouseVerticalSpacer(),
              GreenHouseTextField(controller: _portaController),
              const GreenHouseVerticalSpacer(),
              const GreenHouseVerticalSpacer(height: 30),
              GreenHouseButton(
                color: Colors.brown,
                onPressed: () async {
                  try {
                    try {
                      final service = Get.find<MicroControllerService>();
                      await service.addNewController(
                        GreenHouseMicroControllerFull(
                            hostName: _hostController.text,
                            hostPort: int.parse(_portaController.text),
                            isWorking: false,
                            name: _nomeController.text,
                            userId: Get.find<AuthService>().userId),
                      );
                      Get.back();
                    } catch (err) {
                      throw GreenHouseException.unknown();
                    }
                  } on GreenHouseException catch (err) {
                    Get.snackbar(err.title, err.message);
                  }
                },
                buttonText: 'Conectar',
              ),
            ],
          ),
        ));
  }
}
