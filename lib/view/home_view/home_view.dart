import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/services/auth_service.dart';
import 'package:my_green_house/services/micro_controller_service.dart';
import 'package:my_green_house/view/add_controller_view.dart';

import 'package:my_green_house/view/components/button.dart';
import 'package:my_green_house/view/components/card.dart';
import 'package:my_green_house/view/components/padding.dart';
import 'package:my_green_house/view/components/vertical_spacer.dart';
import 'package:my_green_house/view/detailed_view/bindings.dart';
import 'package:my_green_house/view/detailed_view/detailed_view.dart';

import 'components/status_indicator.dart';

/// Página inicial
///
/// Contém a lista principal das estufas
class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controllerService = Get.find<MicroControllerService>();
    return Scaffold(
      backgroundColor: Colors.green.shade400,
      body: GreenHousePadding(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
          top: 30 + 20,
        ),
        child: SingleChildScrollView(
          child: Obx(
            () => Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GreenHouseStatusIndicator(
                        workingCount: controllerService.controllers
                            .where((controller) => controller.isWorking)
                            .length,
                        notWorkingCount: controllerService.controllers
                            .where((controller) => !controller.isWorking)
                            .length),
                    GreenHouseButton(
                        color: Colors.brown,
                        onPressed: () {
                          Get.to(
                            () => AddControllerView(),
                          );
                        },
                        buttonText: "Adicionar")
                  ],
                ),
                const GreenHouseVerticalSpacer(),
                for (final controller in controllerService.controllers)
                  GreenHouseCard(
                    microController: controller,
                    onPressed: () {
                      Get.to(
                        () => const DetailedView(),
                        binding: DetailedViewBindings(controller.id),
                      );
                    },
                  ),
                const GreenHouseVerticalSpacer(),
                GreenHouseButton(
                    color: Colors.brown,
                    onPressed: () {
                      Get.find<AuthService>().logout();
                    },
                    buttonText: 'Sair')
              ],
            ),
          ),
        ),
      ),
    );
  }
}
