import 'package:flutter/material.dart';
import 'package:my_green_house/view/components/text.dart';

/// Indicador que mostra quantas estufas estão operantes.
class GreenHouseStatusIndicator extends StatelessWidget {
  const GreenHouseStatusIndicator({
    Key? key,
    required this.workingCount,
    required this.notWorkingCount,
  }) : super(key: key);
  final int workingCount;
  final int notWorkingCount;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const GreenHouseText(text: "Minhas Estufas", size: 25),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            _CounterIndicator(
                statusColor: Colors.blue,
                text: "$workingCount ligado${workingCount > 1 ? "s" : ""}"),
            const SizedBox(
              width: 12,
            ),
            _CounterIndicator(
                statusColor: Colors.white,
                text:
                    "$notWorkingCount parada${notWorkingCount > 1 ? "s" : ""}"),
          ],
        ),
      ],
    );
  }
}

class _CounterIndicator extends StatelessWidget {
  const _CounterIndicator(
      {Key? key, required this.statusColor, required this.text})
      : super(key: key);
  final Color statusColor;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 10,
          width: 10,
          decoration: BoxDecoration(
            color: statusColor,
            borderRadius: const BorderRadius.all(
              Radius.circular(180),
            ),
          ),
        ),
        const SizedBox(
          width: 7,
        ),
        Text(text),
      ],
    );
  }
}
