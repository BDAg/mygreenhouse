import 'package:flutter/material.dart';
import 'package:my_green_house/view/components/padding.dart';

/// Página de splash, serve para esperar os serviços serem construídos,
/// enquanto mostra a logo do app.
class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GreenHousePadding(
        child: Center(child: Image.asset("images/green.png")),
      ),
    );
  }
}
