import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:my_green_house/services/auth_service.dart';
import 'package:my_green_house/view/splash_view.dart';
import 'package:get/get.dart';

import 'firebase_options.dart';

/// Função principal
void main() async {
  // Garante que as bindings do flutter sejam inicializadas
  // Precisa chamar esse método antes de tudo.
  WidgetsFlutterBinding.ensureInitialized();

  // Inicializa a instância do firebase.
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  _initServices();
  //Função que faz rodar o app,
  // A tela inicial é a SplashView.
  runApp(GetMaterialApp(
    home: const SplashView(),
    theme: ThemeData(primaryColor: Colors.green.shade400),
  ));
}

/// Função que inica os serviços do app.
void _initServices() {
  Get.put(AuthService());
}
