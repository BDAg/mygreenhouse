import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_green_house/models/exception.dart';
import 'package:my_green_house/services/micro_controller_service.dart';

import '../view/home_view/home_view.dart';
import '../view/landing_view.dart';

/// Classe que representa o serviço de autenticação.
///
/// Contém lógica de login, registro de senha, etc.
///
/// Também gerencia qual página será exibida ao iniciar do app.
class AuthService extends GetxService {
  // Instância do Firebase Auth.
  final _auth = FirebaseAuth.instance;

  // Váriavel que guarda a inscrição de autenticação.
  StreamSubscription? _authListenerSub;

  /// id do usuário atual.
  String get userId => _auth.currentUser!.uid;

  @override
  void onReady() {
    // Registra a inscrição da autenticação.
    _authListenerSub = _auth.authStateChanges().listen(_authListener);
    super.onReady();
  }

  @override
  onClose() {
    // Cancela a inscrição da autenticação.
    _authListenerSub?.cancel();
    super.onClose();
  }

  /// Método que é chamado toda vez que o estado de autenticação mudar.
  /// Caso o usuário esteja autenticado, ir para [HomeView],
  /// Caso contrário ir para [LandingView].
  ///
  /// Também injeta o serviço que cuidará das microcontroladoras quando usuário
  /// está autenticado.
  void _authListener(User? u) {
    if (u != null) {
      // Coloca a dependência na memória.
      Get.put(MicroControllerService());
      // Ir para HomeView e tirar todas páginas do navegador no momento.
      navigator!.pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => const HomeView()),
          // Tira todas as páginas do navegador.
          (route) => false);
    } else {
      // Ir para LandingView e tirar todas páginas do navegador no momento.
      navigator!.pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => const LandingView()),
          // Tira todas as páginas do navegador.
          (route) => false);
    }
  }

  /// Método que faz o login do usuário, precisa de um [email] e [password].
  Future<void> signIn(String email, String password) async {
    try {
      // Tenta fazer o login com o Firebase Auth.
      await _auth.signInWithEmailAndPassword(email: email, password: password);

      // Caso ocorra um FirebaseAuthException
    } on FirebaseAuthException catch (err) {
      // Constrói a mensagem de erro.

      const title = 'Ocorreu um erro ao fazer o login';
      switch (err.code) {
        case 'invalid-email':
          throw const GreenHouseException(
            title: title,
            message: 'O e-mail digitado é inválido',
          );
        case 'user-disabled':
          throw const GreenHouseException(
            title: title,
            message: 'O usuário está desativado',
          );
        case 'user-not-found':
          throw const GreenHouseException(
            title: title,
            message: 'Usuário não encontrado',
          );
        case 'wrong-password':
          throw const GreenHouseException(
            title: title,
            message: 'A senha digitada está incorreta',
          );
      }
      // Caso seja um erro não definido, jogue um erro desconhecido.
      throw GreenHouseException.unknown();
    } catch (err) {
      throw GreenHouseException.unknown();
    }
  }

  /// Método que cria um usuário, precisa de um [email] e [password].
  Future<void> signUp(String email, String password) async {
    try {
      // Tenta criar a conta com o Firebase Auth.
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);

      // Caso ocorra um FirebaseAuthException
    } on FirebaseAuthException catch (err) {
      // Constrói a mensagem de erro.

      const title = 'Ocorreu um erro ao criar a conta';
      switch (err.code) {
        case 'email-already-in-use':
          throw const GreenHouseException(
            title: title,
            message: 'O e-mail já está em uso.',
          );
        case 'invalid-email':
          throw const GreenHouseException(
            title: title,
            message: 'O e-mail digitado é inválido',
          );
        case 'operation-not-allowed':
          throw const GreenHouseException(
            title: title,
            message: 'Operação inválida.',
          );
        case 'weak-password':
          throw const GreenHouseException(
            title: title,
            message: 'A senha é muito fraca para concluir o cadastro.',
          );
      }

      // Caso seja um erro não definido, jogue um erro desconhecido.
    } catch (err) {
      throw GreenHouseException.unknown();
    }
  }

  /// Método que manda um email de esquecer senha, precisa de um [email].
  Future<void> forgetPassword(String email) async {
    try {
      // Tenta criar a conta com o Firebase Auth.
      await _auth.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException {
      // Constrói a mensagem de erro.
      throw const GreenHouseException(
        title: 'Ocorreu um erro ao resetar a senha',
        message: '',
      );

      // Caso seja um erro não definido, jogue um erro desconhecido.
    } catch (err) {
      throw GreenHouseException.unknown();
    }
  }

  /// Método que desloga o usuário atual.
  ///
  /// Também deleta o serviço [MicroControllerService].
  Future<void> logout() async {
    try {
      // Deleta o serviço.
      await Get.delete<MicroControllerService>(force: true);
      // Desloga o usuário atual.
      await _auth.signOut();
    } catch (err) {
      throw GreenHouseException.unknown();
    }
  }
}
