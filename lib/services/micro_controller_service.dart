import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:my_green_house/models/micro_controller.dart';
import 'package:my_green_house/services/auth_service.dart';

import '../util/print.dart';

/// Serviço que gerencia todos os microcontroladores.
///
/// Guarda em uma lista, [controllers]
/// e ouve as mudanças do db.
class MicroControllerService extends GetxService {
  /// Váriavel que indica o nome da coleção no DB
  /// que armazena as controladoras.
  static const colName = "controllers";

  /// Campo no documento que indica qual usuário a controladora pertence.
  static const userIdField = 'userId';

  /// Armazena as controladoras em uma lista reativa
  /// (atualiza a UI conforme necessário).
  final controllers = <GreenHouseMicroController>[].obs;

  // Instância do DB.
  final _db = FirebaseFirestore.instance;
  // Váriavel que guarda a inscrição do DB.
  StreamSubscription? _controllersSub;
  @override
  void onReady() {
    // Pega o id do usuário procurando pelo AuthService.
    final userId = Get.find<AuthService>().userId;
    devPrint("userId: $userId");
    // Inscreve para todas as mudanças que ocorrer no DB.
    _controllersSub = _db
        .collection(colName)
        .where(userIdField, isEqualTo: userId)
        .snapshots()
        .listen(
      _controllerSubListener,
      // Caso ocorra um erro, printar o erro.
      onError: (err) {
        devPrint(err);
      },
    );
    super.onReady();
  }

  @override
  void onClose() {
    // Cancela a inscrição no fechamento desse serviço.
    _controllersSub?.cancel();
    super.onClose();
  }

  /// Método chamado toda vez que houver uma alteração no DB.
  /// Responsável por atualizar a lista de controladoras [controllers] e seus
  /// estados.
  void _controllerSubListener(QuerySnapshot<Map<String, dynamic>> event) {
    // Cria uma lista temporária para armazenar as controladoras
    final newList = <GreenHouseMicroController>[];
    devPrint('received new snapshot');
    // Para cada documento na lista de documentos do DB
    for (final doc in event.docs) {
      try {
        // Tente adicionar um novo objeto GreenHouseMicroController na lista.
        newList.add(GreenHouseMicroController.fromMap(doc.data())..id = doc.id);
      } catch (err) {
        // Caso ocorra um erro, printe o erro.
        devPrint(err);
      }
    }
    // Limpar a lista antiga de controladoras.
    controllers.clear();

    // Transfere a lista antiga para a lista controllers
    for (final controller in newList) {
      controllers.add(controller);
    }

    // Ordena para mostrar as controladoras que estão funcionando
    // em primeiro.
    controllers.sort((a, b) {
      if (a.isWorking && !b.isWorking) {
        return -1;
      }
      return 1;
    });
  }

  /// Método que adiciona uma nova controladora para o DB.
  Future<void> addNewController(
      GreenHouseMicroControllerFull controller) async {
    await _db.collection(colName).add(controller.toMap());
  }
}
