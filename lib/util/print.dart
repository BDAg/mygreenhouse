import 'dart:developer' as dev;

/// Função que envelopa a função log
/// de 'dart:developer'.
/// Usar essa função ao invés de print.
void devPrint(Object? o) {
  dev.log('$o');
}
