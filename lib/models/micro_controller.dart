/// Classe a qual contém dados básicos sobre a controladora.
class GreenHouseMicroController {
  /// uid encontrado no db, será populado por outros serviços.
  late String id;

  /// Nome da controladora
  String name;

  /// Indica se a atual controladora está ou não funcionando.
  bool isWorking;

  /// ID do usuário a qual a controladora pertence.
  String userId;

  /// Classe a qual contém dados básicos sobre a controladora.
  GreenHouseMicroController({
    required this.name,
    required this.isWorking,
    required this.userId,
  });

  /// Retorna a classe em um [Map], útil para salvar no DB.
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'isWorking': isWorking,
      'userId': userId,
    };
  }

  /// Retorna um objeto a partir de um [Map], útil para carregar do DB.
  factory GreenHouseMicroController.fromMap(Map<String, dynamic> map) {
    return GreenHouseMicroController(
      name: map['name'] as String,
      isWorking: map['isWorking'] as bool,
      userId: map['userId'] as String,
    );
  }
}

/// Classe completa sobre o estado da controladora. Contém informações extras
/// que não são necessárias na página inicial
class GreenHouseMicroControllerFull extends GreenHouseMicroController {
  /// Host ou IP da controladora.
  String hostName;

  /// Porta da controladora.
  int hostPort;

  /// Contém o estado da controladora. Pode ser nulo.
  GreenHouseMicroControllerState? state;

  /// Classe completa sobre o estado da controladora. Contém informações extras
  /// que não são necessárias na página inicial
  GreenHouseMicroControllerFull({
    required String name,
    required bool isWorking,
    required String userId,
    required this.hostName,
    required this.hostPort,
    this.state,
  }) : super(name: name, isWorking: isWorking, userId: userId);

  /// Retorna a classe em um [Map], útil para salvar no DB.
  @override
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'isWorking': isWorking,
      'userId': userId,
      'hostName': hostName,
      'hostPort': hostPort,
      'state': state?.toMap(),
    };
  }

  /// Retorna um objeto a partir de um [Map], útil para carregar do DB.
  factory GreenHouseMicroControllerFull.fromMap(Map<String, dynamic> map) {
    return GreenHouseMicroControllerFull(
      name: map['name'] as String,
      isWorking: map['isWorking'] as bool,
      userId: map['userId'] as String,
      hostName: map['hostName'] as String,
      hostPort: map['hostPort'] as int,
      state: map['state'] != null
          ? GreenHouseMicroControllerState.fromMap(
              map['state'] as Map<String, dynamic>)
          : null,
    );
  }
}

/// Classe que guarda o estado atual da controladora.
class GreenHouseMicroControllerState {
  /// Guarda a temperatura da controladora.
  String temp;

  /// Guarda a umidade da controladora.
  String humidity;

  /// Indica o estado da janela.
  bool janela;

  /// Indica o estado da irrigação.
  bool irrigacao;

  /// Classe que guarda o estado atual da controladora.
  GreenHouseMicroControllerState({
    required this.temp,
    required this.humidity,
    required this.janela,
    required this.irrigacao,
  });

  /// Retorna a classe em um [Map], útil para salvar no DB.
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'temp': temp,
      'humidity': humidity,
      'janela': janela,
      'irrigacao': irrigacao,
    };
  }

  /// Retorna um objeto a partir de um [Map], útil para carregar do DB.
  factory GreenHouseMicroControllerState.fromMap(Map<String, dynamic> map) {
    return GreenHouseMicroControllerState(
      temp: map['temp'] as String,
      humidity: map['humidity'] as String,
      janela: map['janela'] as bool,
      irrigacao: map['irrigacao'] as bool,
    );
  }
}
