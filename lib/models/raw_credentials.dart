/// Classe que ajuda a passar as credenciais
/// em um formato cru (raw).
class RawCredentials {
  /// Armazena o email.
  String email;

  /// Armazena a senha.
  String password;

  /// Classe que ajuda a passar as credenciais
  /// em um formato cru (raw).
  RawCredentials(this.email, this.password);
}
