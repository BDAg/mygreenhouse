/// Representa um erro no app.
///
/// Contém um [title] e [message].
///
/// Normalmente usado em conjunto com uma snackbar.
class GreenHouseException implements Exception {
  /// Título do erro
  final String title;

  /// Mensagem descritiva para o erro.
  final String message;

  /// Representa um erro no app.
  ///
  /// Contém um [title] e [message].
  ///
  /// Normalmente usado em conjunto com uma snackbar.
  const GreenHouseException({required this.title, required this.message});

  /// Gera um objeto de erro padrão, váriaveis [title] e [message] já populadas.
  factory GreenHouseException.unknown() => const GreenHouseException(
        title: 'Ocorreu um erro desconhecido',
        message: 'Por favor tente novamente mais tarde.',
      );
}
