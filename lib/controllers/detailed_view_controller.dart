import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:my_green_house/models/micro_controller.dart';
import 'package:my_green_house/services/micro_controller_service.dart';

import '../util/print.dart';

/// Essa classe cuida do estado e gerenciamento das microcontroladoras
/// na página de detalhes.
class DetailedViewController extends GetxController {
  /// Id da controladora para ser acessada no banco de dados
  String contrId;

  /// Essa classe cuida do estado e gerenciamento das microcontroladoras
  /// na página de detalhes.
  DetailedViewController(this.contrId);

  // Instância do banco de dados
  final _db = FirebaseFirestore.instance;

  /// Variável que guarda o objeto da microcontroladora, pode ser nulo.
  Rx<GreenHouseMicroControllerFull?> microController = Rx(null);

  // Variável que guarda a inscrição do listener do DB.
  StreamSubscription? _microControllerSub;
  @override
  void onInit() {
    // "Ouve" as mudanças no db.
    _microControllerSub = _db
        .collection(MicroControllerService.colName)
        .doc(contrId)
        .snapshots()
        .listen(_microControllerListener);
    super.onInit();
  }

  /// Método que é chamado toda vez que o documento muda no DB.
  /// Responsável pora atualizar a variável [microController]
  ///
  /// Também adiciona um estado [GreenHouseMicroControllerState], para fins de teste.
  void _microControllerListener(DocumentSnapshot<Map<String, dynamic>> doc) {
    try {
      // Tentar construir o objeto a partir do snapshot do documento.
      final newContr = GreenHouseMicroControllerFull.fromMap(doc.data()!)
        ..id = doc.id;

      // Fins de teste: cria um estado e envia para o DB.
      if (newContr.state == null) {
        newContr.state = GreenHouseMicroControllerState(
            temp: '35 C', humidity: '45%', janela: true, irrigacao: true);

        devPrint('updating doc...');
        doc.reference.update(newContr.toMap());
      }

      // Atualiza a variavel microController, que notifica a View para mudanças
      microController.value = newContr;
    } catch (err) {
      devPrint(err);
    }
  }

  @override
  void onClose() {
    // Cancela a inscrição, para que não haja vazamento de memória.
    _microControllerSub?.cancel();
    super.onClose();
  }
}
