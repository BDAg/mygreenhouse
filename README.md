# My Green House

## Sobre o Projeto

O projeto visa a construção de um aplicativo de monitoramento de casas de vetegação com o propósito de tornar a agricultura mais econômica e sustentável. Ajudando assim, a impulsionar a produção, aumentar a produtividade e reduzir o desperdício.

- #### **Para acessar a documentação do projeto: [clique aqui](https://gitlab.com/BDAg/mygreenhouse/-/wikis/home)**

## Equipe

- Alan Tomé de Oliveira [ Perfil ](https://gitlab.com/alantome2002)

- Fabio Alexandre de Gênova Filho [ Perfil ](https://gitlab.com/fagenova)

- Leticia Viscardi [ Perfil ](https://gitlab.com/leticia_viscardi)

## Andamento do Projeto

- [X] Entrega 01 - Tema e Equipe
- [X] Entrega 02 - MVP e Lista de Funcionalidades
- [X] Entrega 03 - Mapa de Conhecimento e Cronograma
- [X] Entrega 04 - Montagem de Ambiente
- [X] Entrega 05 - Sprint 1
- [X] Entrega 06 - Sprint 2
- [X] Entrega 07 - Sprint 3



## Preparando Ambiente de Desenvolvimento

#### Instalando Visual Studio Code:

```sh
Baixar Instalador no site https://code.visualstudio.com/

```

#### Instalando Flutter:

```sh
Baixar Instalador no site https://docs.flutter.dev/get-started/install

```
